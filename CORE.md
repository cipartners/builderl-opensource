Command Summary:
---------

### apt ###
apt installs an OS package with the package manager dpkg

    [{apt, "tomcat7"}]

### apt ###
apt removes an OS package with the package manager dpkg

    [{apt, "tomcat7"}, remove]

### chmod ###
Changes permissions of a filesystem object

    [{chmod, "FilesystemObject"}, {permissions, "Permissions"}]

### chown ###
Changes ownership of a filesystem object

    [{chown, "FilesystemObject"}, {owner, "Owner"}]

### Standard UN*X command ###
The easiest form of executing a command is

    [{cmd, "pwd"}]

If you need arguments, write it like this

    [{cmd, "ls"}, {args, "-l"}]

### cmd ###
Run a command with timeout

    [{cmd, Command}, {timeout, Timeout}]

### cp ###
copies a file from one location on the remote machine to another location on the remote machine

    [{cp, "./home/vagrant/apache-ode-war-1.3.6.zip"}, {to, "/tmp"}]

### curl ###
curl downloads a file from an URL, either in the current user directory like this

    [{curl, "http://apache.mirrors.tds.net/ode/apache-ode-war-1.3.6.zip"}]

and if you need the file is a specific location, a destination can be specified

    [{curl, "http://apache.mirrors.tds.net/ode/apache-ode-war-1.3.6.zip"}, {to, "./home/vagrant"}]

### debconf ###
Sets parameters for scripted installs of packages to prevent dialog boxes from popping up

    [{debconf_set, Settings}]

### fetch  ###
Downloads a remote file to a given location

    [{download, "remotefile"}, {to, "local_location"}]

### external ###
Running an external installer

    [{external, InstallerNameAsAtom}, {function, Function}],

### file_append ###
Appends a phrase to a given text file

    [{file_append, Filename}, {phrase, Phrase}]

### file_touch ###
Touches a file as the current user

    [{file_touch, Filename}]

### file_touch ###
Touches a file as root

    [{file_touch, Filename}, {sudo, true}]

### install ###
Install package on server using the default installer as setup in _server_config.cfg

    [{install, Package}] or [{install, Package}, {timeout, Timeout}]

### long_run_script ###
The idea is to have a script running in the background that takes a long time to finish, but not blocking the builder *** needs some thoughts ***

    [{long_run_script, Script}]

### mkdir ###
Creates a (also nested) directory on the remote server

    [{mkdir, "DirectoryName"}]

### mv ###
moves a file from one location on the remote machine to another location on the remote machine

    [{mv, "/tmp/apache-ode-war-1.3.6.zip"}, {to, "/tmp/ode.zip"}]

### replace ###
Replaces a string within a file, the replacement is done in place, no temporary files are needed

    [{replace, Filename}, {search, Search}, {replace, Replace}]

### rm ###
Removes a file from the remote system

    [{rm, Filename}]

### unzip ###
unzips a file in place

    [{unzip, "/tmp/ode.zip"}]

or to a specific location

    [{unzip, "/tmp/ode.zip"}, {to, "/tmp" }]

### upload ###
Uploads a local file to the remote server

    [{upload, "localfile"}, {to, "remote_location"}]

### service ###
Changes the state of a service

    [{service, "Service"}, {state, "start|stop|status|restart|reload"}]

### service ###
Changes the state of a service to either enabled or disabled

    [{service, "Service"}, {status, "enabled|disabled"}]

