MySQL Commands
--------------

### create_database ###
Creates a database using as the current user

    [{create_database, DatabaseName}]

### create_database ###
Creates a database within a (root) password protected database

    [{create_database, DatabaseName}, {password, Password}]

### create_user ###
Creates a database user within a database

    [{create_user, Username}]

### create_user ###
Creates a database user within a password protected database

    [{create_user, Username}, {password, Password}]

### create_user ###
Creates a database user with password within a password protected database

    [{create_user, Username}, {user_pass, UserPass}, {password, Password}]

### drop_database ###
Drops a non-password protected database

    [{drop_database, DatabaseName}]

### drop_database ###
Drops a password protected database

    [{drop_database, DatabaseName}, {password, Password}]

### drop_table ###
Drops a table from a password protected database

    [{drop_table, Table}, {database, Database}, {password, Password}]

### export ###
Exports a password protected database into a file

    [{export, Database}, {file, Filename}, {password, Password}]

### export ###
Exports a username and password protected database into a file

    [{export, Database}, {file, Filename}, {user, User}, {password, Password}]

### export ###
Exports a database into a file

    [{export, Database}, {file, Filename}]

### grant_all ###
Grants all rights to a database user within a password protected database

    [{grant_all, Database}, {dbuser, Username}, {password, Password}]

### import ###
Restores a password protected database from file

    [{import, Filename}, {database, Database}, {password, Password}]

### import ###
Restores a database from file

    [{import, Filename}, {database, Database}]

### mysql ###
Deals with the mysql daemon; start, stop or restart.  No other actions are allowed.

    [{mysql, Action}], _Config) when Action == start ; Action == stop ; Action == restart ->

### query ###
Queries a table within a password protected database

    [{query, Query}, {database, Database}, {password, Password}]

### root_password ###
Sets the root password for MySQL and echoes it on screen

    [{root_password, RootPassword}]

### setrootpwd ###
Sets the root password for MySQL without echoing it on screen

    [{setrootpwd, Password}]

### show_grants ###
Shows the rights of a database user within a password protected database

    [{show_grants, DBUser}, {password, Password}]
