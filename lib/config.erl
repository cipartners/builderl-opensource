-module(config).
-export([
         amazon/1,
         version/0
        ]).

-include("include/global.hrl").
-define(VERSION, "AMAZON Config " ++ ?AMAZON_CONFIG_TAG).

version() ->
    ?VERSION.

amazon(sam) ->
    [{username, "AMAZON_USER_NAME"},
     {access_key_id, "AMAZON_ACCESS_KEY_ID"},
     {secret_access_key, "AMAZON_SECRET_ACCESS_KEY"}];
amazon(kai) ->
    [{username, "ANOTHER_AMAZON_USER_NAME"},
     {access_key_id, "ANOTHER_AMAZON_ACCESS_KEY_ID"},                                                                                                                          
     {secret_access_key, "ANOTHER_AMAZON_SECRET_ACCESS_KEY3"}];
amazon(buildserver) ->
    "ec2.amazonaws.com".
