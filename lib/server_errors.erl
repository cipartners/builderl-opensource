-module(server_errors).
-export([
         errors/1
        ]).


errors({cant_start_instance, Instance}) ->
    io:format("The instance '~s' did not start in time.~n", [Instance]);
errors(list_empty) ->
    io:format("The list of instances is empty~n");
errors({terminate, now}) ->
    io:format("Terminating receive loop~n").
