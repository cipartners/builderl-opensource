-module(mysql_installer).
-export([runner/3, version/0]).

-include("include/global.hrl").
-define(VERSION, "MySQL Installer " ++ ?TAG).

version() ->
    ?VERSION.

echo(Cmd) ->
    io:format("Executing now: ~s~n", [Cmd]).

result(Where, Step) ->
    Cmd = [Step,"\n"],
    echo(Cmd),
    ec2ssh:ssh(Where, Cmd).

result(quiet, Where, Step) ->
    ec2ssh:ssh(Where, [Step,"\n"]).

%% run_step(Where, [{install, Packages}], Config) ->
%%     OSInstaller = proplists:get_value(os_install_cmd, Config, "apt-get"),
%%     InstallerArgs = proplists:get_value(installer_args, Config, "--yes"),
%%     Cmd = io_lib:format("sudo ~s install ~s ~s", [OSInstaller, InstallerArgs, Packages]),
%%     result(Where, Cmd);

%% run_step(Where, [{uninstall, Version}], Config) ->
%%     OSInstaller = proplists:get_value(os_install_cmd, Config, "apt-get"),
%%     InstallerArgs = proplists:get_value(installer_args, Config, "--yes"),
%%     Cmd = io_lib:format("sudo ~s remove ~s ~s", [OSInstaller, InstallerArgs, Version]),
%%     result(Where, Cmd);

run_step(Where, [{setrootpwd, Password}], _Config) ->
    Cmd = io_lib:format("sudo mysqladmin -u root password ~s", [Password]),
    io:format("Setting root password.~n"),
    result(quiet, Where, Cmd);

run_step(Where, [{mysql, Action}], _Config) when Action == start ; Action == stop ; Action == restart ->
    Cmd = io_lib:format("sudo service mysqld ~s", [Action]),
    result(Where, Cmd);
run_step(_Where, [{mysql, UnsupportedAction}], _Config) ->
    io:format("Unsupported action '~s'.~nNot executed.~n", [UnsupportedAction]);

run_step(Where, [{create_database, DatabaseName}], _Config) ->
    Cmd = io_lib:format("sudo mysqladmin -u root create ~s", [DatabaseName]),
    io:format("Creating database '~s'.~n", [DatabaseName]),
    result(Where, Cmd);
run_step(Where, [{create_database, DatabaseName}, {password, Password}], _Config) ->
    Cmd = io_lib:format("sudo mysqladmin -u root --password=~s create ~s", [Password, DatabaseName]),
    io:format("Creating database '~s'.~n", [DatabaseName]),
    result(quiet, Where, Cmd);
run_step(Where, [{drop_database, DatabaseName}], _Config) ->
    Cmd = io_lib:format("sudo mysqladmin -u root drop ~s", [DatabaseName]),
    result(Where, Cmd);
run_step(Where, [{drop_database, DatabaseName}, {password, Password}], _Config) ->
    Cmd = io_lib:format("sudo mysqladmin --force -u root --password=~s drop ~s", [Password, DatabaseName]),
    io:format("Dropping database '~s'.~n(~s)~n", [DatabaseName, Cmd]),
    result(quiet, Where, Cmd);
run_step(Where, [{root_password, RootPassword}], _Config) ->
    Cmd = io_lib:format("sudo mysqladmin -u root password ~s", [RootPassword]),
    result(Where, Cmd);
run_step(Where, [{create_user, Username}], _Config) ->
    Cmd = io_lib:format("sudo mysql -u root -e 'CREATE USER ~s;'", [Username]),
    result(Where, Cmd);
run_step(Where, [{create_user, Username}, {password, Password}], _Config) ->
    Cmd = io_lib:format("sudo mysql -u root --password=~s -e 'CREATE USER ~s;'", [Password, Username]),
    result(Where, Cmd);
run_step(Where, [{create_user, Username}, {user_pass, UserPass}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root --password=~s -e \"CREATE USER '~s'@'%' IDENTIFIED BY '~s';\"", [Password, Username, UserPass])),
    io:format("Creating User: '~s'~n", [Cmd]),
    result(Where, Cmd);

run_step(Where, [{grant_all, Database}, {dbuser, Username}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root --password=~s -e \"GRANT ALL ON ~s.* TO '~s'@'localhost';\"", [Password, Database, Username])),
    result(Where, Cmd);
run_step(Where, [{show_grants, DBUser}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root --password=~s -e \"SHOW GRANTS FOR ~s;\"", [Password, DBUser])),
    result(Where, Cmd);

run_step(Where, [{export, Database}, {file, Filename}], _Config) ->
    Cmd = io_lib:format("sudo mysqldump --user=root ~s > ~s", [Database, Filename]),
    result(Where, Cmd);
run_step(Where, [{export, Database}, {file, Filename}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysqldump -u root --password=~s ~s > ~s", [Password, Database, Filename])),
    result(Where, Cmd);
run_step(Where, [{export, Database}, {file, Filename}, {user, User}, {password, Password}], _Config) ->
    Cmd = io_lib:format("sudo mysqldump --user=~s --password=~s ~s > ~s", [User, Password, Database, Filename]),
    result(Where, Cmd);

run_step(Where, [{import, Filename}, {database, Database}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root ~s < ~s", [Database, Filename])),
    result(Where, Cmd);
run_step(Where, [{import, Filename}, {database, Database}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root --password=~s ~s < ~s", [Password, Database, Filename])),
    result(Where, Cmd);

run_step(Where, [{query, Query}, {database, Database}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root --password=~s ~s -e \"~s\"", [Password, Database, Query])),
    result(Where, Cmd);

run_step(Where, [{drop_table, Table}, {database, Database}, {password, Password}], _Config) ->
    Cmd = lists:flatten(io_lib:format("sudo mysql -u root --password=~s ~s -e \"drop table ~s\"", [Password, Database, Table])),
    result(Where, Cmd);

%% This will try to run any function that did not pattern match
%% with any of the above function heads with the core library.
run_step(Where, Step, Config ) ->
		core_installer:runner(Where, [Step], Config).

runner(Where, Steps, Config) ->
    RunStep = fun(Step) -> run_step(Where, Step, Config) end,
    lists:map(RunStep, Steps).
