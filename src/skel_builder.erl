-module(skel_builder).
-export([
         version/0,
         install/1,
         steps/1,
         uninstall/1
        ]).

-include("include/global.hrl").
-define(VERSION, "Skeleton Builder " ++ ?SKELETON_TAG).
-define(CONFIGFILE, "config/skeleton_config.cfg").

version() ->
    ?VERSION.

install({private_key, PrivateConfig}) ->
    {ok, [Config]} = file:consult(?CONFIGFILE),
    io:format("~s:install({private_key, PrivateConfig)~nPrivateConfig: ~p~nConfig: ~p~n", [?MODULE, PrivateConfig, Config]),
    case proplists:get_value(steps_file, Config) of
        [] ->
            skel_installer:runner(PrivateConfig, steps(), Config);
        StepsFile ->
            io:format("Reading file '~s' for installation instructions~n", [StepsFile]),
            skel_installer:runner(PrivateConfig, steps(StepsFile), Config)
    end;
install(Target) ->
    Comment = proplists:get_value(comment, Target,
                                  "Running against " ++ proplists:get_value(hostname, Target)),
    Where = [{hostname, proplists:get_value(hostname, Target)},
             {username, proplists:get_value(username, Target)},
             {password, proplists:get_value(password, Target)}],
    {ok, Config} = file:consult(?CONFIGFILE),
    io:format("~s ~s~nConfig: ~p~n", [version(), Comment, Config]),
    skel_installer:runner(Where, steps(), Config).

uninstall(Target) ->
    Comment = proplists:get_value(comment, Target,
                                  "Running against " ++ proplists:get_value(hostname, Target)),
    Where = [{hostname, proplists:get_value(hostname, Target)},
             {username, proplists:get_value(username, Target)},
             {password, proplists:get_value(password, Target)}],
    {ok, Config} = file:consult(?CONFIGFILE),
    io:format("~s ~s~nConfig: ~p~n", [version(), Comment, Config]),
    skel_installer:runner(Where, removal_steps(), Config).

steps(Filename) ->
    {ok, [Steps]} = file:consult(Filename),
    proplists:get_value(steps, Steps, []).

steps() ->
    Skeleton = "application",
    [
     [{install, Skeleton}]
		].

removal_steps() ->
    Skeleton = "application",
    [
     [{uninstall, Skeleton}]
    ].
